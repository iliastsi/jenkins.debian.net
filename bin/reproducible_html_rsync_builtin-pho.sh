#!/bin/bash
# vim: set noexpandtab:

# Copyright 2020-2023 Holger Levsen <holger@layer-acht.org>
# released under the GPLv2

DEBUG=false
. /srv/jenkins/bin/common-functions.sh
common_init "$@"

# common code for tests.reproducible-builds.org
. /srv/jenkins/bin/reproducible_common.sh

echo Copying builtin-pho results from ionos7 to jenkins to be used for https://tests.reproducible-builds.org/debian
# rsync builtin-pho results from ionos7
for SUITE in ${SUITES} ; do
	echo "Rsyncing $SUITE."
	rsync -av ionos7-amd64.debian.net:/var/lib/jenkins/builtin-pho-html/debian/$SUITE/ $BASE/debian/$SUITE/
done

